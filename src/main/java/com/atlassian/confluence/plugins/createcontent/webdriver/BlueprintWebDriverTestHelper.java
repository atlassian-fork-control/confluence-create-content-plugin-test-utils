package com.atlassian.confluence.plugins.createcontent.webdriver;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.pageobjects.component.header.Breadcrumbs;
import com.atlassian.confluence.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.plugins.createcontent.pageobjects.CreateDialog;
import com.atlassian.confluence.plugins.createcontent.pageobjects.DashboardPage;

import org.hamcrest.Matcher;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class BlueprintWebDriverTestHelper
{
    private final User user;
    private final ConfluenceRpc rpc;
    private final ConfluenceTestedProduct product;
    private final String createDialogWebItemKey;

    public BlueprintWebDriverTestHelper(ConfluenceRpc rpc, ConfluenceTestedProduct product, User user, String createDialogWebItemKey)
    {
        this.rpc = rpc;
        this.product = product;
        this.user = user;
        this.createDialogWebItemKey = createDialogWebItemKey;
    }

    private CreateDialog openCreateDialog()
    {
        return product.visit(DashboardPage.class).openCreateDialog();
    }

    public <T> T openCreateDialogAndChooseBlueprint(Class<T> pageClass)
    {
        return selectBlueprint(openCreateDialog(), pageClass);
    }

    private CreateDialog loginAndOpenCreateDialog()
    {
        DashboardPage dashboardPage = product.login(user, DashboardPage.class);
        return dashboardPage.openCreateDialog();
    }

    public CreatePage loginAndChooseBlueprint()
    {
        return loginAndChooseBlueprint(CreatePage.class);
    }

    public <T> T loginAndChooseBlueprint(Class<T> pageClass)
    {
        final CreateDialog createDialog = loginAndOpenCreateDialog();
        return selectBlueprint(createDialog, pageClass);
    }

    private <T> T selectBlueprint(CreateDialog createDialog, Class<T> pageClass)
    {
        createDialog.selectContentType(createDialogWebItemKey);
        return createDialog.submit(pageClass);
    }

    public void assertEditorErrorMessageAndFix(CreatePage editor, Matcher<String> errorMessageMatcher)
    {
        assertThat(editor.getErrorMessages(), contains(errorMessageMatcher));

        final String fixedTitle = editor.getTitle() + " but different";
        editor.setTitle(fixedTitle);
        final ViewPage savedPage = editor.save();
        assertThat(savedPage.getTitle(), is(fixedTitle));
    }

    public ViewPage saveEditorAndCheckIndexPageContent(CreatePage editor, String blueprintBreadcrumb, Matcher<String> indexPageContentMatcher)
    {
        final ViewPage newPage = editor.save();
        return goToIndexPageAndCheckContent(newPage, blueprintBreadcrumb, indexPageContentMatcher);
    }

    public ViewPage goToIndexPageAndCheckContent(ViewPage viewPage, String blueprintBreadcrumb, Matcher<String> indexPageContentMatcher)
    {
        // Will ensure that the Index page, when we view it, contains this new page.
        rpc.flushIndexQueue();

        Breadcrumbs breadcrumbs = viewPage.getBreadcrumbs();

        assertThat(breadcrumbs.getLastTextBreadcrumb(), is(blueprintBreadcrumb));

        ViewPage indexPage = breadcrumbs.clickLinkBreadcrumb(breadcrumbs.getBreadcrumbCount() - 1, ViewPage.class);
        assertThat(indexPage.getTitle(), is(blueprintBreadcrumb));
        assertThat(indexPage.getTextContent(), indexPageContentMatcher);
        return indexPage;
    }
}
