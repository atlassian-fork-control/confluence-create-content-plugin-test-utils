package com.atlassian.confluence.plugins.createcontent.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Subclass of the standard Dashboard page object, providing access to the Create button
 */
public class DashboardPage extends com.atlassian.confluence.pageobjects.page.DashboardPage
{
    private static final By CREATE_BUTTON_LOCATOR = By.id("create-page-button");

    public WebElement getCreateButton()
    {
        return driver.findElement(CREATE_BUTTON_LOCATOR);
    }

    public CreateDialog openCreateDialog()
    {
        getCreateButton().click();
        return pageBinder.bind(CreateDialog.class);
    }
}
